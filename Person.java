package object_and_classes;

//We define or design a blueprint of an Object which is called a "class". A blueprint specifies or defines what property and methods should an object have.
public class Person {
	//instance variable:
		//an object has properties(attributes) and methods: 
		//the following variables are the properties of class Person which later be Object Person.
	//properties/attributes
	public int id = 100;
	private String name;
	private int age;
	private double salary;
	private boolean citizen;
	
	public Person() {
		//this is a default or empty constructor.
		//notice there is no class name before Person.
		// there is no Data-type or void method before Person. A constructor doesn't have a return statement
	}
	public Person(String initialName, int age, double initialSalary, boolean initialCitizenshiptStatus){
		//Here you connect the class/instance variables with the constructors variable:
		//if you the same variable name in the constructor, you may use "this" keyword. compare age with others:
		name = initialName;
		this.age = age;
		salary = initialSalary;
		citizen = initialCitizenshiptStatus;
	}
	
	//Methods:
	
	//setter method which doesn't return value back, but remember it does change the status of the instance variable if data is passed to it.
	public void setName(String name) {
		this.name = name;
//		name = personName;
	}
	//Getter method with return a value type string which is retried from the instance variable name;
	public String getName() {
		return this.name;
	}
	//You may see setters and getters without access-specifiers:
			void setage(int age) {
				this.age = age;
			}
			int getAge() {
				return this.age;
			}
	
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public boolean isCitizen() {
		return citizen;
	}
	public void setCitizen(boolean citizen) {
		this.citizen = citizen;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
//	menhaj0102@gmail.com
	
	
	

}
