package object_and_classes;

public class PersonTester {

	public static void main(String[] args) {
		//let's create Object from the defined class.
		//An empty constructor allows to create an object without initial data so you can set it later.
		
		Person james = new Person(); //Object james is created:
		//Note: These setters and getters here are the Interface of Person object: 
		james.setName("James"); //james's ( which is an Object here) name variable(james.name) property is set to "James"
		james.setage(30); //interface of james object. it sets the value or change the state of the objects.
		james.setSalary(25.25);;james.setCitizen(true); 
		james.id = 2300;
		
		
		//To read it and access the object(james) we use the getters method
	
		james.getName();
		james.getAge();
		james.getSalary();
		
		String jName = james.getName(); // read the below print line:
		int jAge = james.getAge();
		System.out.println(jName + "   " + jAge);
		
		//Note: Person's Object states changes as the value or data changes. James has a different state than Tina.1
		
		//In order to print them in the console
		System.out.println(
				"Name: "+ jName +
				" Age: " +james.getAge()+
				" Salary: "+ james.getSalary()+
				" Citizen: "+ james.isCitizen()
				);
		
		
	Person tina = new Person("Tina", 20, 30.49, false);
	//Why the new key here? we are instantiating another a new object from the blueprint(class). We have already set or modeled what we want from the instantiation of the objects.
	System.out.println(tina.getName() +"  "+tina.getAge());
		tina.setName("Jina");
		
	System.out.println(
				"Name: " + tina.getName()
				+ " Age: "+ tina.getAge()
				+ " Salary: "+tina.getSalary()
				+ " Citizen: "+tina.isCitizen()
//				+ "  "+
				);
	
	CustomerSSN jamesSSN = new CustomerSSN(100, "30-33-4949");
	System.out.println(jamesSSN.getId() + "  "+jamesSSN.getSsn() );
	
	}//main method bracket
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
