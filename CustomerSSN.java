package object_and_classes;

public class CustomerSSN {
	private int id;
	private String ssn;
	
	public CustomerSSN(int id, String ssn) {
		this.id = id;
		this.ssn = ssn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	

}
